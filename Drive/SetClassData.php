<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Attachment\Drive;

trait SetClassData
{
    public function setParam(array $data)
    {
        foreach (get_class_vars(get_class($this)) as $name => $value) {
            $this->$name = $data[$this->unCamelize($name)] ?? $value;
        }
    }
    
    protected function unCamelize(string $name)
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', '$1' . '_' . '$2', $name));
    }
}
