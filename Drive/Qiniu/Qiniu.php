<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Attachment\Drive\Qiniu;

use Psr\SimpleCache\CacheInterface;
use Qiniu\Auth;
use Qiniu\Http\Client;
use Qiniu\Http\Error;
use Qiniu\Storage\BucketManager;
use Qiniu\Storage\UploadManager;
use Throwable;
use W7\Attachment\Drive\BaseDrive;
use W7\Attachment\Exception\AttachmentException;

class Qiniu extends BaseDrive
{
    /** @var Config */
    protected $config;

    /** @var CacheInterface|null */
    protected $cache = null;

    /**
     * Qiniu constructor.
     * @param Config $config 七牛云配置
     * @param string|null $cache 实现CacheInterface缓存的门面类完整命名空间
     */
    public function __construct(Config $config, ?string $cache = null)
    {
        $this->config = $config;
        if (is_subclass_of($cache, CacheInterface::class)) {
            $this->cache = $cache;
        }
    }

    /**
     * @return Auth
     */
    public function getBase()
    {
        return $this->getAuth();
    }

    /**
     * 上传文件
     * @param string $filePath 要上传的文件
     * @param string $fileName 文件名
     * @param string|null $directory 目录 实际上，七牛云不存在目录这个概念
     * @return bool
     * @throws AttachmentException
     */
    public function putFile(string $filePath, string $fileName, ?string $directory = null): bool
    {
        $upToken = $this->getUploadToken();
        $upload  = new UploadManager();
        if (null === $directory) {
            $key = $fileName;
        } else {
            $directory = $this->toUnixPath($directory);
            $key       = $directory . '/' . $fileName;
        }
        try {
            $upload->putFile($upToken, $key, $filePath);
            return true;
        } catch (Throwable $e) {
            throw new AttachmentException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function delete(string $filePath): bool
    {
        $bucketManager = new BucketManager($this->getAuth());
        /** @var Error $result */
        list($err, $result) = $bucketManager->delete($this->config->bucket, $this->toUnixPath($filePath));
        if (null !== $err) {
            throw new AttachmentException($err);
        }
        if (null !== $result) {
            if (200 !== $result->getResponse()->statusCode) {
                throw new AttachmentException($result->getResponse()->error);
            }
        }

        return true;
    }

    public function getFile(string $filePath)
    {
        $response = Client::get($this->getPublicUrl($filePath));
        if (200 === $response->statusCode) {
            return $response->body;
        }
        throw  new AttachmentException($response->error);
    }

    public function getPrivateUrl(string $filePath): string
    {
        $auth = $this->getAuth();
        $url  = $this->getPublicUrl($filePath);
        return $auth->privateDownloadUrl($url);
    }

    public function getPublicUrl(string $filePath): string
    {
        return $this->config->baseUrl . '/' . $this->toUnixPath($filePath);
    }

    protected function getAuth()
    {
        return new Auth($this->config->accessKey, $this->config->secretKey);
    }

    protected function getUploadToken()
    {
        $expires  = 7200;
        $cacheKey = 'file:qiniu:uploadToken';
        $upToken  = '';
        if (null !== $this->cache) {
            if ($this->cache->has($cacheKey)) {
                $upToken = $this->cache->get($cacheKey);
            }
        }

        if (empty($upToken)) {
            $upToken = $this->getAuth()->uploadToken($this->config->bucket, null, $expires, null, true);
            $this->cache?->set($cacheKey, $upToken, $expires);
        }
        return $upToken;
    }
}
