<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Attachment\Drive\Qiniu;

use W7\Attachment\Drive\SetClassData;

class Config
{
    use SetClassData;
    
    public $accessKey;
    
    public $secretKey;
    
    public $bucket;
    
    public $baseUrl;
    
    public function __construct(string $accessKey = '', string $secretKey = '', string $bucket = '', string $baseUrl = '')
    {
        $this->accessKey = $accessKey;
        $this->secretKey = $secretKey;
        $this->bucket    = $bucket;
        $this->baseUrl   = $baseUrl;
    }
}
