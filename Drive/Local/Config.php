<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Attachment\Drive\Local;

use W7\Attachment\Drive\SetClassData;

class Config
{
    use SetClassData;
    
    public $baseUrl;

    public $basePath;
    
    public function __construct(string $baseUrl = null, string $basePath = null)
    {
        $this->baseUrl  = $baseUrl;
        $this->basePath = $basePath;
    }
}
