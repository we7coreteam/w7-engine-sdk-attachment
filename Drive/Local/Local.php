<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Attachment\Drive\Local;

use W7\Attachment\Drive\BaseDrive;
use W7\Attachment\Exception\AttachmentException;

class Local extends BaseDrive
{
    protected $config;
    
    public function __construct(Config $config)
    {
        $this->config = $config;
    }
    
    public function getBase()
    {
        return null;
    }
    
    public function putFile(string $filePath, string $fileName, ?string $directory = null): bool
    {
        if (!empty($this->config->basePath)) {
            $directory = $this->config->basePath . DIRECTORY_SEPARATOR . $directory;
        }
        if (null !== $directory) {
            if (!is_dir($directory)) {
                if (false === @mkdir($directory, 0777, true) && !is_dir($directory)) {
                    throw new AttachmentException(sprintf('Unable to create the "%s" directory', $directory));
                }
            } elseif (!is_writable($directory)) {
                throw new AttachmentException(sprintf('Unable to write in the "%s" directory', $directory));
            }
        }
        if (null === $directory) {
            $key = $fileName;
        } else {
            $key = $directory . DIRECTORY_SEPARATOR . $fileName;
        }
        set_error_handler(function ($type, $msg) use (&$error) {
            $error = $msg;
        });
        
        if (file_exists($key)) {
            $this->delete($key);
        }
        
        if (0 === strpos($filePath, '/tmp')) {
            $result = move_uploaded_file($filePath, $key);
        } else {
            $result = rename($filePath, $key);
        }
        restore_error_handler();
        if (!$result) {
            throw new AttachmentException(sprintf('Could not move the file "%s" to "%s" (%s)', $filePath, $directory, strip_tags($error)));
        }
        @chmod((string) $key, 0666 & ~umask());
        return true;
    }
    
    public function delete(string $filePath): bool
    {
        $filePath = $this->config->basePath . DIRECTORY_SEPARATOR . str_replace('/', DIRECTORY_SEPARATOR, $filePath);
        if (file_exists($filePath)) {
            return @unlink($filePath);
        }
        return true;
    }
    
    public function getFile(string $filePath)
    {
        $filePath = $this->config->basePath . DIRECTORY_SEPARATOR . str_replace('/', DIRECTORY_SEPARATOR, $filePath);
        return file_get_contents($filePath);
    }

    /**
     * 获取文件访问Url
     * @param string $filePath
     * @return string
     */
    public function getPrivateUrl(string $filePath): string
    {
        return $this->config->baseUrl . '/' . $this->toUnixPath($filePath);
    }

    public function getPublicUrl(string $filePath): string
    {
        return $this->getPrivateUrl($filePath);
    }
}
