<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Attachment\Drive\Oss;

use OSS\Core\OssException;
use OSS\OssClient;
use W7\Attachment\Drive\BaseDrive;
use W7\Attachment\Exception\AttachmentException;

class Oss extends BaseDrive
{
    protected $config;
    
    protected $oss;
    
    public function __construct(Config $config)
    {
        $this->config = $config;
        try {
            $this->oss = new OssClient($this->config->accessKeyId, $this->config->accessSecret, $this->config->endpoint);
        } catch (OssException $e) {
            throw new AttachmentException($e->getMessage(), $e->getCode(), $e);
        }
    }
    
    public function getBase()
    {
        return $this->oss;
    }
    
    public function putFile(string $filePath, string $fileName, ?string $directory = null): bool
    {
        if (null === $directory) {
            $key = $fileName;
        } else {
            $directory = $this->toUnixPath($directory);
            $key       = $directory . '/' . $fileName;
        }
        
        try {
            $this->oss->uploadFile($this->config->bucket, $key, $filePath);
        } catch (OssException $e) {
            throw new AttachmentException($e->getMessage(), $e->getCode(), $e);
        }
        return true;
    }
    
    public function delete(string $filePath): bool
    {
        $this->oss->deleteObject($this->config->bucket, $this->toUnixPath($filePath));
        return true;
    }
    
    public function getFile(string $filePath)
    {
        return $this->oss->getObject($this->config->bucket, $this->toUnixPath($filePath));
    }
    
    public function getPrivateUrl(string $filePath): string
    {
        try {
            return $this->oss->signUrl($this->config->bucket, $this->toUnixPath($filePath), $this->config->timeout);
        } catch (OssException $e) {
            throw new AttachmentException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getPublicUrl(string $filePath): string
    {
        $http_type = ((isset($_SERVER['HTTPS']) && 'on' == $_SERVER['HTTPS']) || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && 'https' == $_SERVER['HTTP_X_FORWARDED_PROTO'])) ? 'https://' : 'http://';
        $baseUrl   = $this->config->baseUrl ?: $http_type . $this->config->bucket . '.' . $this->config->endpoint;
        return $baseUrl . '/' . $this->toUnixPath($filePath);
    }
}
