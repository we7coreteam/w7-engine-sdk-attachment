<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Attachment\Drive\Oss;

use W7\Attachment\Drive\SetClassData;

class Config
{
    use SetClassData;

    /**
     * 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
     * @var string
     */
    public $accessKeyId;
    
    /**
     * @var string
     */
    public $accessSecret;
    
    /**
     * 地域节点,您选定的OSS数据中心访问域名
     * @var string
     */
    public $endpoint;
    
    /**
     * 存储空间名称
     * @var string
     */
    public $bucket;
    
    /**
     * 私有URL访问有效期，单位(秒)
     * @var int
     */
    public $timeout = 7200;

    /**
     * 自定义访问Url
     * @var string
     */
    public $baseUrl = '';
    
    public function __construct(string $accessKeyId = '', string $accessSecret = '', string $endpoint = '', string $bucket = '', string $baseUrl = '', int $timeout = 7200)
    {
        $this->accessKeyId  = $accessKeyId;
        $this->accessSecret = $accessSecret;
        $this->endpoint     = $endpoint;
        $this->bucket       = $bucket;
        $this->baseUrl      = $baseUrl;
        $this->timeout      = $timeout;
    }
}
