<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Attachment\Drive;

use W7\Attachment\Contracts\RemoteFile;

abstract class BaseDrive implements RemoteFile
{
    public function toUnixPath(string $path)
    {
        return str_replace('\\', '/', $path);
    }
}
