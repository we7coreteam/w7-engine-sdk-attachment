<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Attachment\Drive\Cos;

use Qcloud\Cos\Client;
use Throwable;
use W7\Attachment\Drive\BaseDrive;
use W7\Attachment\Exception\AttachmentException;

class Cos extends BaseDrive
{
    /** @var Config */
    protected $config;
    
    /** @var Client */
    protected $cos;
    
    public function __construct(Config $config)
    {
        $this->config = $config;
        $this->cos    = new Client([
            'region'      => $this->config->region,
            'credentials' => [
                'secretId'  => $this->config->secretId,
                'secretKey' => $this->config->secretKey
            ]
        ]);
    }
    
    public function getBase()
    {
        return $this->cos;
    }
    
    public function putFile(string $filePath, string $fileName, ?string $directory = null): bool
    {
        if (null === $directory) {
            $key = $fileName;
        } else {
            $directory = $this->toUnixPath($directory);
            $key       = $directory . '/' . $fileName;
        }
        
        if (!file_exists($filePath)) {
            throw new AttachmentException("file ${$filePath} does not exist");
        }
        $file = fopen($filePath, 'rb');
        try {
            $result = $this->cos->upload($this->config->bucket, $key, $file);
        } catch (Throwable $e) {
            throw new AttachmentException($e->getMessage(), $e->getCode(), $e);
        }

        return (bool)$result;
    }
    
    public function delete(string $filePath): bool
    {
        try {
            $result = $this->cos->deleteObject([
                'Bucket' => $this->config->bucket,
                'Key'    => $this->toUnixPath($filePath)
            ]);
        } catch (Throwable $e) {
            throw new AttachmentException($e->getMessage(), $e->getCode(), $e);
        }
        
        return (bool)$result;
    }
    
    public function getFile(string $filePath)
    {
        try {
            $result = $this->cos->getObject([
                'Bucket' => $this->config->bucket,
                'Key'    => $this->toUnixPath($filePath)
            ]);
        } catch (Throwable $e) {
            throw new AttachmentException($e->getMessage(), $e->getCode(), $e);
        }
        return $result['Body'];
    }
    
    public function getPrivateUrl(string $filePath): string
    {
        return $this->cos->getObjectUrl($this->config->bucket, $this->toUnixPath($filePath), '+' . $this->config->timeout . ' seconds');
    }

    public function getPublicUrl(string $filePath): string
    {
        $filePath = $this->toUnixPath($filePath);
        if (empty($this->config->baseUrl)) {
            return 'https://' . $this->config->bucket . '.cos.' . $this->config->region . '.myqcloud.com/' . $filePath;
        }
        return $this->config->baseUrl . '/' . $filePath;
    }
}
