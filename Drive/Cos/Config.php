<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Attachment\Drive\Cos;

use W7\Attachment\Drive\SetClassData;

class Config
{
    use SetClassData;

    /**
     * 云 API 密钥 SecretId
     * @var string
     */
    public $secretId;
    
    /**
     * 云 API 密钥 SecretKey
     * @var string
     */
    public $secretKey;
    
    /**
     * 存储桶地域
     * @var string
     */
    public $region;
    
    /**
     * 存储桶名称 格式：BucketName-APPID
     * @var string
     */
    public $bucket;
    
    /**
     * 私有URL访问有效期，单位(秒)
     * @var int
     */
    public $timeout = 7200;

    /**
     * 访问Url
     * @var string
     */
    public $baseUrl = '';

    /**
     * Config constructor.
     * @param string $secretId  云 API 密钥 SecretId
     * @param string $secretKey 云 API 密钥 SecretKey
     * @param string $region    存储桶地域
     * @param string $bucket    存储桶名称 格式：BucketName-APPID
     * @param string $baseUrl   访问Url
     * @param int $timeout      私有URL访问有效期，单位(秒)
     */
    public function __construct(string $secretId = '', string $secretKey = '', string $region = '', string $bucket = '', string $baseUrl = '', int $timeout = 7200)
    {
        $this->secretId  = $secretId;
        $this->secretKey = $secretKey;
        $this->region    = $region;
        $this->bucket    = $bucket;
        $this->baseUrl   = $baseUrl;
        $this->timeout   = $timeout;
    }
}
