<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Attachment\Drive\Ftp;

use FtpClient\FtpClient;
use FtpClient\FtpException;
use W7\Attachment\Drive\BaseDrive;
use W7\Attachment\Exception\AttachmentException;

class Ftp extends BaseDrive
{
    protected $config;
    
    protected $ftp;
    
    public function __construct(Config $config)
    {
        $this->config = $config;
        $this->ftp    = new FtpClient();
        try {
            $this->ftp->connect($this->config->host, $this->config->ssl, $this->config->port, $this->config->timeOut <= 0 ? 90 : $this->config->timeOut);
            $this->ftp->login($this->config->user, $this->config->pass);
            $this->ftp->set_option(FTP_USEPASVADDRESS, false);
            $this->ftp->pasv($this->config->passive);
            $this->ftp->chdir($this->config->basePath);
        } catch (FtpException $e) {
            throw new AttachmentException($e->getMessage(), $e->getCode(), $e);
        }
    }
    
    public function getBase()
    {
        return $this->ftp;
    }
    
    public function putFile(string $filePath, string $fileName, ?string $directory = null): bool
    {
        if (null !== $directory) {
            $directory = $this->toUnixPath($directory);
            if (false === @$this->ftp->chdir($directory)) {
                $this->ftp->mkdir($directory, true);
                if (false === @$this->ftp->chdir($directory)) {
                    throw new AttachmentException('Directory creation failed');
                }
            }
        }

        return $this->ftp->put($fileName, $filePath, FTP_BINARY);
    }
    
    public function delete(string $filePath): bool
    {
        return $this->ftp->delete($this->toUnixPath($filePath));
    }
    
    public function getFile(string $filePath)
    {
        return $this->ftp->getContent($this->toUnixPath($filePath));
    }

    /**
     * 获取文件访问Url
     * @param string $filePath
     * @return string
     */
    public function getPrivateUrl(string $filePath): string
    {
        return $this->config->baseUrl . '/' . $this->toUnixPath($filePath);
    }

    public function getPublicUrl(string $filePath): string
    {
        return $this->getPrivateUrl($filePath);
    }
}
