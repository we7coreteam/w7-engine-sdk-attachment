<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Attachment\Drive\Ftp;

use W7\Attachment\Drive\SetClassData;

class Config
{
    use SetClassData;
    
    public $host;
    
    public $user;
    
    public $pass;
    
    public $baseUrl;
    
    public $ssl = false;
    
    public $port = 21;
    
    public $passive = false;
    
    public $basePath = '/';

    public $timeOut = 90;
    /**
     * Config constructor.
     * @param string $host       FTP 地址
     * @param string $user       Ftp 账号
     * @param string $pass       Ftp 密码
     * @param string $baseUrl    Ftp 远程访问Url
     * @param string $basePath   Ftp 根目录
     * @param int $port          Ftp 端口
     * @param bool $passive      Ftp 被动模式
     * @param bool $ssl          FTP 是否启用SSL连接
     * @param int $timeOut       FTP 链接超时时间
     */
    public function __construct(
        string $host = '',
        string $user = '',
        string $pass = '',
        string $baseUrl = '',
        string $basePath = '/',
        int $port = 21,
        bool $passive = false,
        bool $ssl = false,
        int $timeOut = 90
    ) {
        $this->host     = $host;
        $this->user     = $user;
        $this->pass     = $pass;
        $this->baseUrl  = $baseUrl;
        $this->basePath = $basePath;
        $this->port     = $port;
        $this->passive  = $passive;
        $this->ssl      = $ssl;
        $this->timeOut  = $timeOut;
    }
}
