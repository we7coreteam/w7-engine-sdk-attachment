<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Attachment\Contracts;

use W7\Attachment\Exception\AttachmentException;

interface RemoteFile
{
    /**
     * 获取远程附件的SDK实类
     * @return mixed
     */
    public function getBase();
    /**
     * 上传文件
     * @param string $filePath 本地文件绝对路径
     * @param string $fileName 远程文件名字
     * @param string|null $directory 远程路径
     * @return bool
     * @throws AttachmentException
     */
    public function putFile(string $filePath, string $fileName, ?string $directory = null): bool;
    
    /**
     * 删除文件
     * @param string $filePath 远程附件路径
     * @return bool
     * @throws AttachmentException
     */
    public function delete(string $filePath): bool;
    
    /**
     * 获取文件
     * @param string $filePath 远程附件路径
     * @return array
     * @throws AttachmentException
     */
    public function getFile(string $filePath);
    
    /**
     * 获取文件私有访问Url
     * @param string $filePath 远程附件路径
     * @return string
     * @throws AttachmentException
     */
    public function getPrivateUrl(string $filePath): string;

    /**
     * 获取文件公开访问Url
     * @param string $filePath
     * @return string
     */
    public function getPublicUrl(string $filePath): string;
}
